FROM python:3.7
WORKDIR /code
ADD ./code/requirements.txt /code/requirements.txt
RUN pip install -r requirements.txt

